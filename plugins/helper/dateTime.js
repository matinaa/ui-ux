/*
* npm i moment moment-timezone
*/


import moment from "moment-jalaali";
import momentZone from 'moment-timezone'

const timeZone = {
  getRegion() {
    return momentZone.tz.guess();
  },
  timeByZone(input, outFormat = null, inFormat = null) {
    //not empty input
    if (!input) return null;

    //set default out format
    if (!outFormat) outFormat = 'HH:mm:ss';

    //set default in format
    if (!inFormat) inFormat = 'HH:mm:ss';

    let localeTimeZone = momentZone.tz.guess();
    return momentZone(input, inFormat).tz(localeTimeZone).format(outFormat)
  }

};
const dateTime = {
  install(Vue) {
    Vue.mixin({
      data: () => ({

        momentSet: null,

      }),
      created: function () {
        moment.loadPersian({usePersianDigits: false, dialect: 'persian-modern'});
        this.momentSet = moment();
      },
    });
    Vue.prototype.$isValidDate = function (val = null) {
      let date = moment(val);
      return !!(date.isValid() && (date.format('YYYY') > 1990));
    };
    Vue.prototype.$getDate = function (input, outFormat = null, inFormat = null) {
      //not empty input
      if (!input) return null;

      //set default out format
      if (!outFormat) outFormat = 'jYYYY/jMM/jDD  -  HH:mm';

      //set default in format
      if (!inFormat) inFormat = 'YYYY-MM-DDTHH:mm:ssZ';

      //return result

      let date = moment(input);
      if (date.isValid() && (date.format('YYYY') > 1990))
        return moment(input, inFormat).format(outFormat);
      else
        return 'invalid'
    };
    Vue.prototype.$moment = function (val = null) {
      return moment(val)
    };
    Vue.prototype.$getYear = function (outFormat = null) {
      let format = outFormat;
      if (!outFormat)
        format = 'YYYY-MM-DDTHH:mm:ssZ';
      return {
        start: this.momentSet.startOf('jYear').format(format),
        end: this.momentSet.endOf('jYear').format(format),
      }
    };
    Vue.prototype.$getMonth = function (outFormat = null) {
      let format = outFormat;
      if (!outFormat)
        format = 'YYYY-MM-DDTHH:mm:ssZ';
      return {
        start: this.momentSet.startOf('jMonth').format(format),
        end: this.momentSet.endOf('jMonth').format(format),
      }
    };
    Vue.prototype.$timeZone = timeZone;
  }
};
export default dateTime;

