// This is your plugin object. It can be exported to be used anywhere.
const currency = {
  // The install method is all that needs to exist on the plugin object.
  // It takes the global Vue object as well as user-defined options.
  install(Vue) {

    Vue.prototype.$commas = function (val, commas = false) {
      if (val) {
        let res = val.replace(/,/g, '');
        if (commas)
          return this.addCommas(res)
        else
          return res
      }else return null
    };

    Vue.prototype.$getIRT = function (val, commas = true, postfix = true, customPostfix = null) {

      let res = val;
      if (!res) return null;

      //commas
      if (commas) res = this.addCommas(res);

      //postfix
      if (postfix) res = res + ' ' + ((customPostfix) ? customPostfix : 'تومان');

      return res;
    };

    Vue.prototype.$getIRR = function (val, commas = true, postfix = true, customPostfix = null) {
      let res = val;

      //commas
      if (commas) res = this.addCommas(res);

      //postfix
      if (postfix) res = res + ' ' + ((customPostfix) ? customPostfix : 'ریال');

      return res;
    };

    // We call Vue.mixin() here to inject functionality into all components.
    Vue.mixin({
      // Anything added to a mixin will be injected into all components.
      // In this case, the mounted() method runs when the component is added to the DOM.
      methods: {
        addCommas(nStr) {
          if(!nStr)
            return 0

          nStr += '';
          let x = nStr.split('.');
          let x1 = x[0];
          let x2 = x.length > 1 ? '.' + x[1] : '';
          let rgx = /(\d+)(\d{3})/;
          while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
          }
          return x1 + x2;
        }
      }
    });
  }
};
export default currency;
