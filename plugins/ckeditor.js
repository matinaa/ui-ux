import Vue from 'vue';
import CKEditor from '@ckeditor/ckeditor5-vue';


export default () => {
    Vue.use( CKEditor );
}


