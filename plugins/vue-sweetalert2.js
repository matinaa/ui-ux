import Vue from 'vue'
import VueSweetalert2 from 'vue-sweetalert2';

import swal from 'sweetalert2/dist/sweetalert2.all.min.js'

Vue.use(VueSweetalert2);