export const state = () => ({
    dataList: [],
    groupList:[]
})

export const actions = {
    getDataDictionaryGroup({commit,state}) {
        return new Promise((resolve, reject) => {
            if(!state.groupList.length)
            {
                this.$axios.get('/api/datadictionary/groups/all',{
                    headers: {
                        locale: 'fa',
                    }
                })
                    .then(resp => {
                        commit('set_data_dictionary_group', resp.data.result)
                        resolve()
                    }).catch(err => reject(err))
            }else
                resolve()
        })
    },
    getDataDictionary({commit,state}) {
        return new Promise((resolve, reject) => {
            if(!state.dataList.length)
            {
                this.$axios.get('/api/datadictionary/all',{
                    headers: {
                        locale: 'fa',
                    }
                })
                    .then(resp => {
                        commit('set_data_dictionary', resp.data.result)
                        resolve()
                    }).catch(err => reject(err))
            }else
                resolve()
        })
    },
    //
    async update({commit,state}) {
        return new Promise(async (resolve, reject) => {

            try{

                let respG = await this.$axios.get('/api/datadictionary/groups/all',{headers: {locale: 'fa'}})
                commit('set_data_dictionary_group', respG.data.result)

                let respD = await this.$axios.get('/api/datadictionary/all',{headers: {locale: 'fa'}})
                commit('set_data_dictionary', respD.data.result)

                resolve()

            }catch (e) {
                //window.location.reload()
            }

        })
    },
}

export const mutations = {
    set_data_dictionary_group(state, items) {
        state.groupList = items
    },
    set_data_dictionary(state, items) {
        state.dataList = items
    },
}

export const getters = {
    getData: (state) => (key) => {
        let groupId = null

        //get group id
        for(let group of state.groupList)
            if(group.key==key)
                groupId = group.id

        let res = [];
        for(let data of state.dataList)
            if(data.GroupId==groupId)
                res.push(data)
        return res;
    }
}
