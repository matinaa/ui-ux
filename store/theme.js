export const state = () => ({
    wideSideNav: true,
})

export const actions = {
    async toggleSideNav({commit, state}) {
        commit('SET_SIDE_NAV', !state.wideSideNav)
    },
}

export const mutations = {
    SET_SIDE_NAV(state, status) {
        state.wideSideNav = status
    },
}

export const getters = {}
