export const state = () => ({
    list: [],
    isRtl: false,
})

export const actions = {
    getAll({commit, rootState}) {
        return new Promise((resolve, reject) => {
            this.$axios.get('/api/General/SiteLanguages')
                .then(resp => {
                    commit('set', resp.data.result)
                    resolve()
                }).catch(err => resolve())
        })
    },
    setDir({commit}) {

        document.body.className = 'rtl';
        commit('SET_DIR', true)

        /*if (this.app.i18n.locale === 'fa') {
            document.body.className = 'rtl-style';
            $('#sidenav-main').addClass('fixed-right')
            $('#sidenav-main').removeClass('fixed-left')
            commit('SET_DIR', true)
        } else if (this.app.i18n.locale === 'en' || this.app.i18n.locale === 'es' || this.app.i18n.locale === 'sv') {
            document.body.className = 'ltr-style';
            $('#sidenav-main').addClass('fixed-left')
            $('#sidenav-main').removeClass('fixed-right')
            commit('SET_DIR', false)
        }*/
    },
}

export const mutations = {
    SET_DIR(state, status) {
        state.isRtl = status
    },
    set(state, items) {
        state.list = items
    },
}
